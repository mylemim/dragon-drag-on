﻿using UnityEngine;

namespace Common.Movement
{
    public class UnidirectionalMovement : MonoBehaviour
    {
        public float Speed = 1f;

        /// <summary>
        /// Tolerance when determining if the object is out of the initial position
        /// </summary>
        public float PositionAdjustmentTolerance = 0.1f;

        public Vector2 directionVector = Vector2.right;

        private Vector2 initialPosition;

        private void OnEnable()
        {
            initialPosition = transform.position;
        }

        void Start()
        {
            directionVector = directionVector.normalized;
        }

        void Update()
        {
            float currentYOffset = initialPosition.y - transform.position.y;
            Vector2 movementVector = directionVector;

            if (Mathf.Abs(currentYOffset) > PositionAdjustmentTolerance)
            {
                Vector2 movementOffset = new Vector2(0, currentYOffset);
                movementOffset.Normalize();
                movementVector = movementVector + movementOffset;
            }

            gameObject.transform.Translate(movementVector * Speed * Time.deltaTime);
        }

        public void SetDirectionVector(Vector2 directionVector)
        {
            this.directionVector = directionVector.normalized;
        }
    }
}
