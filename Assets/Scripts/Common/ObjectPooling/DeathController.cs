﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System;
using Common.ObjectPooling.Death;

namespace Common.ObjectPooling
{
    public class DeathController : MonoBehaviour
    {
        public UnityEvent OnObjectDeath;

        private IDeathStrategy deathStrategy = new DefaultDeathStrategy();

        public void SetDeathStrategy(IDeathStrategy deathStrategy)
        {
            this.deathStrategy = deathStrategy;
        }

        public virtual void Die()
        {
            //Notify all listeners about the death
            OnObjectDeath.Invoke();

            deathStrategy.Die(gameObject);

            //Clear all listeners for reusability of the object in the pool
            RemoveAllListeners();
        }

        public virtual void RemoveAllListeners()
        {
            OnObjectDeath.RemoveAllListeners();
        }
    }
}
