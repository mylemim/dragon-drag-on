﻿using UnityEngine;
using System.Collections;
using System;

namespace Common.ObjectPooling.Spawn
{
    public class InfiniteSpawnStrategy : SpawnStrategy
    {
        public override bool IsNextObjectAvailable()
        {
            return true;
        }

        public override void NextObjectInstantiated()
        {
            //NOOP
        }

        public override void OnSpawnedObjectDestroyed()
        {
            //NOOP
        }
    }
}
