﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Events;

namespace Common.ObjectPooling.Spawn
{
    /// <summary>
    /// Provides strategy for generation of limited number of objects
    /// </summary>
    public class LimitedSpawnStrategy : SpawnStrategy
    {
        public int GameObjectsToInstantiate = 10;
        [Space]
        public UnityEvent OnAllEnemiesDestroyed;

        private int instantiatedObjects;
        private int destroyedGameObjects;

        [ExecuteInEditMode]
        void OnValidate()
        {
            GameObjectsToInstantiate = Mathf.Abs(GameObjectsToInstantiate);
        }

        void Start()
        {
            instantiatedObjects = 0;
            destroyedGameObjects = 0;
        }

        public override bool IsNextObjectAvailable()
        {
            return instantiatedObjects < GameObjectsToInstantiate;
        }

        public override void NextObjectInstantiated()
        {
            instantiatedObjects++;
        }

        public override void OnSpawnedObjectDestroyed()
        {
            destroyedGameObjects++;

            if (destroyedGameObjects == GameObjectsToInstantiate)
            {
                OnAllEnemiesDestroyed.Invoke();
            }
        }
    }
}
