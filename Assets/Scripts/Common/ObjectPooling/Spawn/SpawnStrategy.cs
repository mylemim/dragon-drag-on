﻿using UnityEngine;
using System.Collections;

namespace Common.ObjectPooling.Spawn
{
    public abstract class SpawnStrategy : MonoBehaviour
    {
        public abstract bool IsNextObjectAvailable();

        /// <summary>
        /// Notify this strategy another object has been instantiated. This way the strategy may keep state and determine the availability of subsequent objects.
        /// </summary>
        public abstract void NextObjectInstantiated();

        /// <summary>
        /// Called when the spawned object is destroyed. May emit an event depending on the strategy.
        /// </summary>
        public abstract void OnSpawnedObjectDestroyed();
    }
}
