﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System;
using System.Collections.Generic;
using Common.ObjectPooling.Death;
using Common.ObjectPooling.Spawn;

namespace Common.ObjectPooling
{
    public class SpawnManager : MonoBehaviour
    {
        public SpawnLocation[] SpawnLocations;

        /// <summary>
        /// Game object to initialize in warmup for the object pool
        /// </summary>
        [Space]
        public int GameObjectsToInitialize = 10;

        public SpawnStrategy SpawnStrategy;

        public SpawnDecorator[] decorators;

        public GameObject ObjectPrefab;
        public GameObject EnemyContainer;

        /// <summary>
        /// Waiting time between each object spawn in seconds.
        /// </summary>
        public float WaitingTimeBetweenSpawns
        {
            get
            {
                return waitingTimeBetweenSpawns;
            }

            set
            {
                waitingTimeBetweenSpawns = Mathf.Abs(value);
            }
        }

        /// <summary>
        /// Inverse of waiting time between spawn. Indicates number of objects spawned each second.
        /// </summary>
        public float SpawnFrequency
        {
            get
            {
                return 1f / waitingTimeBetweenSpawns;
            }

            set
            {
                waitingTimeBetweenSpawns = 1f / Mathf.Abs(value);
            }
        }

        private float lastSpawnTime;
        private ObjectPool objectPool;

        [HideInInspector]
        [SerializeField]
        private float waitingTimeBetweenSpawns = 5;

        [ExecuteInEditMode]
        void OnValidate()
        {
            GameObjectsToInitialize = Mathf.Abs(GameObjectsToInitialize);

            if (!SpawnStrategy)
            {
                throw new Exception("Spawn strategy not set");
            }

            if (!ObjectPrefab)
            {
                throw new Exception("Object prefab not set");
            }
        }

        void Start()
        {
            if (EnemyContainer == null)
            {
                Debug.Log("EnemyContainer not set, setting this as EnemyContainer");
                EnemyContainer = gameObject;
            }

            objectPool = new ObjectPool(ObjectPrefab);

            //Pre-warm the object pool with predefined objects
            for (int i = 0; i < GameObjectsToInitialize; i++)
            {
                objectPool.PoolObject(CreateObject());
            }

            lastSpawnTime = 0;
        }

        // Update is called once per frame
        void Update()
        {
            if (SpawnLocations.Length > 0 && SpawnStrategy.IsNextObjectAvailable() && Time.time - lastSpawnTime > WaitingTimeBetweenSpawns)
            {
                SpawnObject();
            }
        }

        public GameObject CreateObject()
        {
            SpawnLocation spawnLocation = SpawnLocations[UnityEngine.Random.Range(0, SpawnLocations.Length)];
            GameObject spawnedGameObject = objectPool.CreateObject();

            //Set object inactive, so we can adjust the position and relationships
            //Upon activation, the pooled object may have some behaviors that may be triggered
            spawnedGameObject.SetActive(false);

            spawnLocation.InitializeSpawnedObject(spawnedGameObject);
            foreach (SpawnDecorator decorator in decorators)
            {
                decorator.DecorateSpawnedObject(spawnedGameObject);
            }

            //Reference the target group
            spawnedGameObject.transform.parent = EnemyContainer.transform;

            spawnedGameObject.SetActive(true);

            return spawnedGameObject;
        }

        public GameObject SpawnObject()
        {
            GameObject spawnedGameObject = CreateObject();

            //Add reference to spawn so we can recognize when this object has been destroyed
            DeathController deathController = spawnedGameObject.GetComponent<DeathController>();
            if (deathController)
            {
                deathController.OnObjectDeath.AddListener(SpawnStrategy.OnSpawnedObjectDestroyed);
                deathController.SetDeathStrategy(new PooledObjectDeathStrategy(objectPool));
            }

            lastSpawnTime = Time.time;
            SpawnStrategy.NextObjectInstantiated();

            return spawnedGameObject;
        }
    }
}
