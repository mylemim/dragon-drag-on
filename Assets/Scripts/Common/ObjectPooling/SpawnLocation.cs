﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Common.ObjectPooling
{
    public class SpawnLocation : MonoBehaviour
    {

        public Vector2 direction = Vector2.right;

        /// <summary>
        /// Initializes the game object position and rotation, as well as the desired movement direction for the object
        /// </summary>
        /// <param name="spawnedGameObject">The game object created by the spawn manager</param>
        public void InitializeSpawnedObject(GameObject spawnedGameObject)
        {
            spawnedGameObject.transform.position = transform.position;
            spawnedGameObject.transform.rotation = Quaternion.FromToRotation(Vector2.right, direction);
        }
    }
}
