﻿using UnityEngine;
using System.Collections;

namespace Common.ObjectPooling
{
    public abstract class SpawnDecorator : MonoBehaviour
    {
        public abstract void DecorateSpawnedObject(GameObject spawnedObject);
    }
}
