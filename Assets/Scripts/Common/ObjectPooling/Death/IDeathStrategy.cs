﻿using UnityEngine;

namespace Common.ObjectPooling.Death
{
    public interface IDeathStrategy
    {
        void Die(GameObject gameObject);
    }
}
