﻿using UnityEngine;

namespace Common.ObjectPooling.Death
{
    public class DefaultDeathStrategy : IDeathStrategy
    {
        public void Die(GameObject gameObject)
        {
            GameObject.Destroy(gameObject);
        }
    }
}