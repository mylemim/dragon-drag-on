﻿using UnityEngine;

namespace Common.ObjectPooling.Death
{
    public class PooledObjectDeathStrategy : IDeathStrategy
    {
        ObjectPool pool;

        public PooledObjectDeathStrategy(ObjectPool pool)
        {
            this.pool = pool;
        }

        public void Die(GameObject gameObject)
        {
            pool.PoolObject(gameObject);
        }
    }
}