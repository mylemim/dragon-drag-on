﻿using Common.Game;
using UnityEngine;

namespace Common.DragAndDrop
{
    /// <summary>
    /// Provides drag and drop behavior for buildings and other constructed objects which will be selected from the interface
    /// </summary>
    public class DragAndDropControl : PauseControl
    {
        public GameObject TargetGameObject { get; set; }

        private float distance;
        private bool dragging = false;

        private bool draggingEnabled = true;

        private GameObject triggeredGameObject;

        void Update()
        {
            // Transfer the position of the object if it is being dragged
            if (dragging && draggingEnabled)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                Vector2 rayPoint = (Vector2)ray.GetPoint(distance);
                transform.position = rayPoint;
            }
        }

        void OnMouseDown()
        {
            dragging = true;

            distance = Vector2.Distance(transform.position, Camera.main.transform.position);
        }

        void OnMouseUp()
        {
            dragging = false;

            if (triggeredGameObject != null && draggingEnabled)
            {
                Debug.Log("Snapping to object: " + triggeredGameObject);

                transform.position = triggeredGameObject.transform.position;

                // The snap object will determine the final positioning behavior depending on the decorators provided
                ExecuteSnapObjectDecorator(triggeredGameObject);
            }
        }

        private void ExecuteSnapObjectDecorator(GameObject snappingGameObject)
        {
            DragAndDropDecorator[] dragAndDropDecorators = snappingGameObject.GetComponents<DragAndDropDecorator>();
            foreach (DragAndDropDecorator dragAndDropDecorator in dragAndDropDecorators)
            {
                Debug.Log(dragAndDropDecorator + " executing");
                dragAndDropDecorator.DecorateGameObject(this.gameObject);
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (IsTargetObjectInRange(collision))
            {
                Debug.Log("Withing range of snap object");
            }
        }

        private void OnTriggerStay2D(Collider2D collision)
        {
            // If we are near a building point, we will latch the object onto the point
            if (IsTargetObjectInRange(collision) && triggeredGameObject != collision.gameObject)
            {
                triggeredGameObject = collision.gameObject;
                Debug.Log("Stored game object: " + triggeredGameObject);
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            // If we are near a building point, we will latch the object onto the point
            if (IsTargetObjectInRange(collision) && triggeredGameObject != null)
            {
                Debug.Log("Exiting snap object, clearing state");
                triggeredGameObject = null;
            }
        }

        private bool IsTargetObjectInRange(Collider2D collision)
        {
            return collision.gameObject == TargetGameObject;
        }

        public override void SetGameObjectPaused(bool isPaused)
        {
            draggingEnabled = !isPaused;
            //Set dragging to false so the object does not teleport to the mouse upon unpause
            dragging = false;
        }
    }
}
