﻿using UnityEngine;

namespace Common.DragAndDrop
{
    public abstract class DragAndDropDecorator : MonoBehaviour
    {
        public abstract void DecorateGameObject(GameObject decoratedGameObject);
    }
}
