﻿using Common.ObjectPooling;
using UnityEngine;

namespace Common.DragAndDrop
{
    public class DeletingDragAndDropTarget : DragAndDropDecorator
    {
        public override void DecorateGameObject(GameObject decoratedGameObject)
        {
            DeathController deathController = decoratedGameObject.GetComponent<DeathController>();
            if (deathController != null)
            {
                deathController.Die();
            }
            else
            {
                Destroy(decoratedGameObject);
            }
        }
    }
}
