﻿using UnityEngine;

namespace Common.DragAndDrop
{
    public class OrientatingDragAndDropTarget : DragAndDropDecorator
    {
        [Range(0, 360)]
        public float facingAngle = 0;

        public override void DecorateGameObject(GameObject decoratedGameObject)
        {
            decoratedGameObject.transform.rotation = Quaternion.Euler(0, 0, facingAngle);
        }
    }
}
