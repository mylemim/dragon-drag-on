﻿using UnityEngine;
using System.Collections;
using Common.ObjectPooling;

namespace Common.World
{
    /// <summary>
    /// Will delete or return to the pool an object which is overlapping the world bounds
    /// </summary>
    public class WorldBounds : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D collision)
        {
            GameObject triggeringGameObject = collision.gameObject;

            DeathController deathController = triggeringGameObject.GetComponent<DeathController>();
            if (deathController != null)
            {
                deathController.Die();
            }
            else
            {
                Destroy(triggeringGameObject);
            }
        }
    }
}
