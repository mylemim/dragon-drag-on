﻿using UnityEngine;
using System.Collections;

namespace Common.Game
{
    /// <summary>
    /// Controls the time scaling for the pause function and pauses extended behaviors for PauseControlObjects
    /// </summary>
    /// <see cref="PauseControl"/>
    public class PauseController : MonoBehaviour
    {
        private bool isGamePaused = false;

        public bool IsGamePaused
        {
            get
            {
                return isGamePaused;
            }
        }

        /// <summary>
        /// Toggles the paused state to the opposite state
        /// </summary>
        /// <param name="isPaused"></param>
        public void ToggleGamePaused()
        {
            SetGamePaused(!isGamePaused);
        }

        /// <summary>
        /// Pauses the time in the game and freezes otherwise unpausable behaviors in game objects (such as drag and drop function).
        /// </summary>
        /// <param name="isPaused"></param>
        public void SetGamePaused(bool isPaused)
        {
            Time.timeScale = isPaused ? 0 : 1;
            isGamePaused = isPaused;
            TogglePausableObjects();
        }

        /// <summary>
        /// Depending on the state of the game, enables or disables behaviors in game objects which can be otherwise accessed during the pause
        /// (such as drag and drop).
        /// </summary>
        private void TogglePausableObjects()
        {
            PauseControl[] pausableGameObjects = FindObjectsOfType<PauseControl>();

            foreach (PauseControl pausableObject in pausableGameObjects)
            {
                pausableObject.SetGameObjectPaused(isGamePaused);
            }
        }
    }
}

