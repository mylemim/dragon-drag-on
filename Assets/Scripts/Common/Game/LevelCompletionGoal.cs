﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

namespace Common.Game
{
    /// <summary>
    /// The level completion goal is triggered when a specific condition is reached. Upon reaching either of the existing goals, the level completion controller
    /// tests all existing goals for successful completion. Once all goals are completed, the level is also complete.
    /// </summary>
    public class LevelCompletionGoal : MonoBehaviour
    {
        [SerializeField]
        private bool isGoalReached = false;
        [SerializeField]
        public string Description = "";

        /// <summary>
        /// The event which will trigger when the goal is reached
        /// </summary>
        public UnityEvent OnGoalReached = new UnityEvent();

        public bool IsGoalReached
        {
            get
            {
                return isGoalReached;
            }

            //The goal may be reached only once
            set
            {
                isGoalReached = isGoalReached | value;

                if (isGoalReached)
                {
                    Debug.Log("Goal reached: " + Description);
                    OnGoalReached.Invoke();
                }
            }
        }
    }
}

