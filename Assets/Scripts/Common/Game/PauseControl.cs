﻿using UnityEngine;

namespace Common.Game
{
    /// <summary>
    /// Game object behavior for an object which can be paused and disable a part of its behavior when the pause menu shows
    /// </summary>
    public abstract class PauseControl : MonoBehaviour
    {
        public abstract void SetGameObjectPaused(bool isPaused);
    }
}
