﻿using UnityEngine.Events;

namespace Common.Game.Events
{
    [System.Serializable]
    public class LongValueUpdatedEvent : UnityEvent<long> { }
}
