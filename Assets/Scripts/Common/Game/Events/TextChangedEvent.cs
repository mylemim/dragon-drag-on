﻿using UnityEngine.Events;

namespace Common.Game.Events
{
    [System.Serializable]
    public class TextChangedEvent : UnityEvent<string> { }
}
