﻿using UnityEngine;

namespace Common.Game.Score
{
    /// <summary>
    /// Game object behavior for an object which can increment score value
    /// </summary>
    public class ScoreEvaluator : MonoBehaviour
    {
        public int ScoreValue = 0;

        public int GetScoreValue()
        {
            return ScoreValue;
        }
    }
}
