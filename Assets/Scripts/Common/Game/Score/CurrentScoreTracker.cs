﻿using Common.Game.Events;
using UnityEngine;

namespace Common.Game.Score
{
    public class CurrentScoreTracker : MonoBehaviour
    {
        public long Score
        {
            get
            {
                return score;
            }

            internal set
            {
                score = value;
                NotifyCurrentScoreChanged();
            }
        }

        [SerializeField]
        private long score = 0;

        /// <summary>
        /// Determines if the target score has been achieved or not
        /// </summary>
        [SerializeField]
        private ScoreEvaluationStrategy scoreEvaluationStrategy;

        /// <summary>
        /// The event containing the score text which will be manipulated as the score changes
        /// </summary>
        public TextChangedEvent OnScoreTextChanged = new TextChangedEvent();

        /// <summary>
        /// The event containing the score value which will be manipulated as the score changes
        /// </summary>
        public LongValueUpdatedEvent OnScoreChanged = new LongValueUpdatedEvent();

        private void Start()
        {
            NotifyCurrentScoreChanged();
        }

        /// <summary>
        /// Add score from an object which contains a score value in it
        /// </summary>
        /// <param name="gameObject">The object containing a ScoreEvaluator component</param>
        public virtual void AddScore(GameObject gameObject)
        {
            ScoreEvaluator scorableGameObject = gameObject.GetComponent<ScoreEvaluator>();

            if (scorableGameObject)
            {
                score = scoreEvaluationStrategy.CalculateScoreChange(score, scorableGameObject.GetScoreValue());
                NotifyCurrentScoreChanged();
            }
        }

        private void NotifyCurrentScoreChanged()
        {
            Debug.Log("Current score changed - notifying");
            OnScoreChanged.Invoke(score);
            OnScoreTextChanged.Invoke(score.ToString());
        }
    }
}
