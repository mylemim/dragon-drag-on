﻿using UnityEngine;
using System.Collections;

namespace Common.Game.Score
{
    /// <summary>
    /// Contains methods for changing and evaluating if the score has been achieved depending on the way the score changes
    /// </summary>
    public abstract class ScoreEvaluationStrategy : MonoBehaviour
    {
        /// <summary>
        /// Evaulate current score against the set conditions of the target score and any other overriding conditions
        /// </summary>
        /// <param name="currentScore">The current score</param>
        /// <param name="targetScore">The target score that should be achieved</param>
        /// <returns>If the target score is achieved or not</returns>
        public abstract bool IsScoreAchieved(long currentScore, long targetScore);

        /// <summary>
        /// Evaulate changed score by applying the score delta to the current score
        /// </summary>
        /// <param name="currentScore">The current score</param>
        /// <param name="scoreDelta">The amount the score changes</param>
        /// <returns>The new current score amount</returns>
        public abstract long CalculateScoreChange(long currentScore, long scoreDelta);
    }
}

