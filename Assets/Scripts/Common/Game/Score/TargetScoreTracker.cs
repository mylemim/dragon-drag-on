﻿using UnityEngine;
using Common.Game.Events;
using UnityEngine.Events;

namespace Common.Game.Score
{
    public class TargetScoreTracker : MonoBehaviour
    {
        public long TargetScore
        {
            get
            {
                return targetScore;
            }

            internal set
            {
                targetScore = value;
                NotifyTargetScoreChanged();
            }
        }

        [SerializeField]
        private long targetScore = 300;

        /// <summary>
        /// Determines if the target score has been achieved or not
        /// </summary>
        [SerializeField]
        private ScoreEvaluationStrategy scoreEvaluationStrategy;

        /// <summary>
        /// The event containing the score text which is the targeted score text
        /// </summary>
        public TextChangedEvent OnTargetScoreTextChanged;

        /// <summary>
        /// The event which triggers once the target score has been reached with the amount achieved
        /// </summary>
        public LongValueUpdatedEvent OnScoreAmountAchieved;

        /// <summary>
        /// The event which triggers once the target score has been reached
        /// </summary>
        public UnityEvent OnScoreAchieved;

        // Use this for initialization
        public virtual void Start()
        {
            InitializeTargetScore();
            NotifyTargetScoreChanged();
        }

        /// <summary>
        /// Set the target score to the initial value during the Start phase of the game object
        /// </summary>
        protected virtual void InitializeTargetScore()
        {
        }

        private void NotifyTargetScoreChanged()
        {
            Debug.Log("Target score changed - notifying");
            OnTargetScoreTextChanged.Invoke(targetScore.ToString());
        }

        /// <summary>
        /// Process the currently achieved score
        /// </summary>
        /// <param name="currentScore"></param>
        public void HandleScore(long currentScore)
        {
            if (scoreEvaluationStrategy.IsScoreAchieved(currentScore, targetScore))
            {
                Debug.Log("Firing OnScoreAchieved");
                OnScoreAmountAchieved.Invoke(currentScore);
                OnScoreAchieved.Invoke();
            }
        }
    }
}
