﻿using UnityEngine;
using Common.Game.Events;

namespace Common.Game.Score
{
    public class HighScoreTracker : MonoBehaviour
    {
        public long HighScore
        {
            get
            {
                return highScore;
            }

            internal set
            {
                highScore = value;
            }
        }

        [SerializeField]
        private long highScore = 0;

        /// <summary>
        /// The event containing the high score text
        /// </summary>
        public TextChangedEvent OnHighScoreTextChanged = new TextChangedEvent();

        // Use this for initialization
        private void Start()
        {
            InitializeHighScore();
            NotifyHighScoreChanged();
        }

        /// <summary>
        /// Set the high score to the initial value during the Start phase of the game object
        /// </summary>
        protected virtual void InitializeHighScore()
        {
        }

        private void NotifyHighScoreChanged()
        {
            OnHighScoreTextChanged.Invoke(highScore.ToString());
        }
    }
}
