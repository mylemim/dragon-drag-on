﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System;
using DragonDragOn.Game.Settings;

namespace DragonDragOn.Game
{
    public class GameController : MonoBehaviour
    {
        private static GameController instance;

        private const string saveFolder = "Saves";
        private static readonly string saveFilePath = Path.Combine(saveFolder, "save.bin");

        private const long MinLevel = 1;

        [SerializeField]
        private long currentLevel = MinLevel;

        [SerializeField]
        private PlayerGameData playerGameData;

        [SerializeField]
        private LevelMetadata[] availableLevels = new LevelMetadata[2];

        public bool HasNextLevel
        {
            get { return currentLevel < MaxLevel; }
        }

        public long CurrentLevel
        {
            get { return currentLevel; }
        }

        public LevelMetadata CurrentLevelMetadata
        {
            get { return availableLevels[currentLevel - 1]; }
        }

        public long MaxLevel
        {
            get { return availableLevels.LongLength; }
        }

        public long MaxLevelAchieved
        {
            get { return playerGameData.MaxLevelAchieved; }
        }

        //Awake is always called before any Start functions
        void Awake()
        {
            InitializeInstance();
        }

        private void InitializeInstance()
        {
            //Check if instance already exists and enforce this is the only known instance
            if (instance == null)
            {
                instance = this;

                LoadData();
            }
            else if (instance != this)
            {
                Destroy(gameObject);
            }


            //Sets this to not be destroyed when reloading scene
            DontDestroyOnLoad(gameObject);
        }

        /// <summary>
        /// Increases the latest current level and saves the game data
        /// </summary>
        public void SetCurrentLevelWon()
        {
            if (playerGameData.MaxLevelAchieved == currentLevel)
            {
                playerGameData.MaxLevelAchieved++;
            }

            SaveData();
        }

        public void SetCurrentLevel(long level)
        {
            if (level < MinLevel || level > MaxLevel || level > playerGameData.MaxLevelAchieved)
            {
                throw new Exception("Level out of bounds! Got " + level + ", expected >0, <= " + playerGameData.MaxLevelAchieved);
            }

            currentLevel = level;
        }

        /// <summary>
        /// Save current game setting progress into the game save file
        /// </summary>
        public void SaveData()
        {
            if (!Directory.Exists(saveFolder))
            {
                Directory.CreateDirectory(saveFolder);
            }

            BinaryFormatter formatter = new BinaryFormatter();
            FileStream saveFile = File.Create(saveFilePath);
            formatter.Serialize(saveFile, playerGameData);

            saveFile.Close();
        }

        /// <summary>
        /// Load previously saved game setting progress from the game save file
        /// </summary>
        public void LoadData()
        {
            if (File.Exists(saveFilePath))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                FileStream saveFile = File.Open(saveFilePath, FileMode.Open);
                playerGameData = (PlayerGameData)formatter.Deserialize(saveFile);

                saveFile.Close();
            }
            else
            {
                playerGameData = new PlayerGameData();
            }
        }
    }
}
