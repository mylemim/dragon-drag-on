﻿using Common.Game.Score;

namespace DragonDragOn.Game.Score
{
    public class CountDownScoreStrategy : ScoreEvaluationStrategy
    {
        public override long CalculateScoreChange(long currentScore, long scoreDelta)
        {
            long result = currentScore - scoreDelta;

            if (result < 0)
            {
                return 0;
            }

            return currentScore - scoreDelta;
        }

        public override bool IsScoreAchieved(long currentScore, long targetScore)
        {
            return currentScore <= targetScore;
        }
    }
}

