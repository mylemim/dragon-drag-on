﻿using UnityEngine;
using System;

namespace DragonDragOn.Game.Settings
{
    [Serializable]
    public class SavedLevelData
    {
        [SerializeField]
        private long highScore = 0;
        [SerializeField]
        private bool highScoreSet = false;

        public long HighScore
        {
            get
            {
                return highScore;
            }

            set
            {
                highScore = value;
                highScoreSet = true;
            }
        }

        public bool HighScoreSet
        {
            get
            {
                return highScoreSet;
            }
        }
    }
}
