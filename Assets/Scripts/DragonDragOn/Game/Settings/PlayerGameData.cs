﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace DragonDragOn.Game.Settings
{
    [Serializable]
    public class PlayerGameData
    {
        /// <summary>
        /// Current version of the game settings, used in determining backwards compatibility
        /// </summary>
        [SerializeField]
        private int version = 1;

        [SerializeField]
        internal int MaxLevelAchieved = 1;

        //[SerializeField]
        //private Dictionary<string, SavedLevelData> storedLevelData = new Dictionary<string, SavedLevelData>();
    }
}
