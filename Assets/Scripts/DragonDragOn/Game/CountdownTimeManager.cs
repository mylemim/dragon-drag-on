﻿using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace DragonDragOn.Game
{
    /// <summary>
    /// Keeeps track of the remaining time for the set countdown time. 
    /// Upon the reached time, emits an event notifying all listeners that the time has elapsed.
    /// </summary>
    public class CountdownTimeManager : MonoBehaviour
    {
        /// <summary>
        /// Countdown time expressed in seconds
        /// </summary>
        public float CountdownTime = 10;
        public string timeFormat = "0.0";

        /// <summary>
        /// The game object containing the time text which will be manipulated as the time changes
        /// </summary>
        public TextMeshProUGUI timeText;

        public UnityEvent OnTimeElapsed;

        private float remainingTime;
        private bool countdownInProgress = false;

        private void Awake()
        {
            GameController gameController = FindObjectOfType<GameController>();
            CountdownTime = gameController.CurrentLevelMetadata.CountDownTime;
        }

        // Use this for initialization
        void Start()
        {
            RestartCountdown();
        }

        public void RestartCountdown()
        {
            remainingTime = CountdownTime;
            countdownInProgress = true;

            if (timeText)
            {
                DisplayTime();
            }
            else
            {
                Debug.LogError("Time text destination does not exist! The game will not keep time.");
            }
        }

        void Update()
        {
            if (countdownInProgress)
            {
                remainingTime -= Time.deltaTime;

                if (remainingTime <= 0)
                {
                    SetTimeElapsed();
                }

                DisplayTime();
            }
        }

        private void SetTimeElapsed()
        {
            remainingTime = 0;
            countdownInProgress = false;

            Debug.Log("Firing OnTimeElapsed");
            OnTimeElapsed.Invoke();
        }

        private void DisplayTime()
        {
            if (timeText)
            {
                timeText.text = remainingTime.ToString(timeFormat);
            }
        }
    }
}
