﻿using Common.Game;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor.Events;
using UnityEngine;
using UnityEngine.Events;
using TMPro;
using Common.Game.Score;

namespace DragonDragOn.Game
{
    public class BucketFactory : MonoBehaviour
    {
        public GameObject bucketPrefab;

        public Vector2 StartingBucketPivotPosition;
        public float bucketXPositionOffset;

        private GameController gameController;

        private int instantiatedBuckets = 0;

        private void Awake()
        {
            gameController = FindObjectOfType<GameController>();

            ValidateGoals();
            InstantiateBuckets();
        }

        private void ValidateGoals()
        {
            foreach (LevelGoal currentLevelGoal in gameController.CurrentLevelMetadata.LevelGoals)
            {
                List<LevelGoal> supportedGoals = Enumerable.ToList(from goal in gameController.CurrentLevelMetadata.LevelGoals where goal.TargetDragonType == currentLevelGoal.TargetDragonType select goal);
                if (supportedGoals.Count > 1)
                {
                    throw new System.Exception("Multiple supported goals found for " + currentLevelGoal.TargetDragonType);
                }
                else if (supportedGoals.Count == 0)
                {
                    throw new System.Exception("No supported goals found for " + currentLevelGoal.TargetDragonType);
                }
            }
        }

        private void InstantiateBuckets()
        {
            foreach (LevelGoal currentLevelGoal in gameController.CurrentLevelMetadata.LevelGoals)
            {
                GameObject bucket = Instantiate(bucketPrefab);

                bucket.GetComponent<BucketBehavior>().SupportedDragonType = currentLevelGoal.TargetDragonType;
                bucket.GetComponent<SpriteRenderer>().color = currentLevelGoal.TargetDragonType == DragonType.RED ? Color.red : Color.green;

                LevelCompletionGoal levelCompletionGoal = bucket.GetComponent<LevelCompletionGoal>();
                levelCompletionGoal.Description = currentLevelGoal.TargetDragonType + " Level Goal";

                LevelManager levelManager = FindObjectOfType<LevelManager>();
                UnityEventTools.AddPersistentListener(levelCompletionGoal.OnGoalReached, levelManager.GoalReached);

                bucket.transform.parent = gameObject.transform;
                bucket.transform.position = DetermineBucketPosition();
                bucket.name = currentLevelGoal.TargetDragonType + "Bucket";

                AddScoreText(bucket, currentLevelGoal.NumberOfDragonsToCollect, levelCompletionGoal);
            }
        }

        private Vector2 DetermineBucketPosition()
        {
            // Place the first bucket in the middle
            if (instantiatedBuckets == 0 && gameController.CurrentLevelMetadata.LevelGoals.Length % 2 != 0)
            {
                return StartingBucketPivotPosition;
            }

            // Half of the buckets go left, and half go right;
            int xPrefix = instantiatedBuckets % 2 == 0 ? 1 : -1;
            instantiatedBuckets++;

            return StartingBucketPivotPosition + new Vector2(xPrefix * bucketXPositionOffset, 0);
        }

        private void AddScoreText(GameObject bucket, long targetScore, LevelCompletionGoal levelCompletionGoal)
        {
            TextMeshProUGUI targetText = bucket.GetComponentInChildren<TextMeshProUGUI>();
            TargetScoreTracker targetScoreTracker = bucket.GetComponent<TargetScoreTracker>();
            CurrentScoreTracker currentScoreTracker = bucket.GetComponent<CurrentScoreTracker>();

            // Set to 0 because we want the target score to be count down
            targetScoreTracker.TargetScore = 0;
            currentScoreTracker.Score = targetScore;
        }
    }
}
