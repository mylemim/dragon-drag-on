﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace DragonDragOn.Game
{
    public class LevelSelector : MonoBehaviour
    {
        private GameController gameController;

        public GameObject buttonParent;
        public GameObject buttonPrefab;

        void Awake()
        {
            gameController = FindObjectOfType<GameController>();
        }

        private void Start()
        {
            for (long i = 1; i <= gameController.MaxLevel; i++)
            {
                long level = i;
                GameObject buttonGameObject = GameObject.Instantiate(buttonPrefab);

                buttonGameObject.transform.SetParent(buttonParent.transform, false);
                buttonGameObject.name = "LevelButton" + level;

                buttonGameObject.GetComponentInChildren<TextMeshProUGUI>().SetText("Level " + level);

                Button button = buttonGameObject.GetComponent<Button>();
                button.onClick.AddListener(delegate ()
                {
                    gameController.SetCurrentLevel(level);
                    SceneManager.LoadScene(gameController.CurrentLevelMetadata.SceneName);
                });

                if (level > gameController.MaxLevelAchieved)
                {
                    button.interactable = false;
                }
            }
        }
    }
}
