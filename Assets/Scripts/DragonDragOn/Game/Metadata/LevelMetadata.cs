﻿using UnityEngine;

namespace DragonDragOn.Game
{
    [CreateAssetMenu(menuName = "Level/Metadata")]
    public class LevelMetadata : ScriptableObject
    {
        public string LevelName;
        public string SceneName = "Template_Level";
        public float CountDownTime = 10;

        [SerializeField]
        public LevelGoal[] LevelGoals;
    }
}