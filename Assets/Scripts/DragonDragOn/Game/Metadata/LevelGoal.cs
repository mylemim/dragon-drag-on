﻿using UnityEngine;
using System.Collections;
using System;

namespace DragonDragOn.Game
{
    [Serializable]
    public class LevelGoal
    {
        public DragonType TargetDragonType;
        public long NumberOfDragonsToCollect;
    }
}

