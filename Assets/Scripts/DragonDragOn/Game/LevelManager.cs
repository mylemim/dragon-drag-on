﻿using Common.Game;
using System;
using UnityEngine;

namespace DragonDragOn.Game
{
    public class LevelManager : MonoBehaviour
    {
        private const string PAUSE_KEY = "Cancel";

        private bool isGameWon = false;
        private bool isGameOver = false;

        public GameObject PauseMenu;
        public GameObject WinMenu;
        public GameObject LoseMenu;

        private PauseController pauseController;
        private GameController gameController;

        [ExecuteInEditMode]
        void OnValidate()
        {
            if (!PauseMenu)
            {
                throw new Exception("No pause menu set");
            }
        }

        private void Awake()
        {
            pauseController = gameObject.AddComponent<PauseController>();
            gameController = FindObjectOfType<GameController>();
        }

        private void Start()
        {
            PauseMenu.SetActive(false);
            WinMenu.SetActive(false);
            LoseMenu.SetActive(false);
            SetGamePaused(false);
        }

        private void Update()
        {
            if (Input.GetButtonDown(PAUSE_KEY) && !isGameOver)
            {
                SetGamePaused(!pauseController.IsGamePaused);
            }
        }

        public void GoalReached()
        {
            Debug.Log("Goal reached, searching for other goals");
            LevelCompletionGoal[] goals = FindObjectsOfType<LevelCompletionGoal>();
            bool result = true;

            foreach (LevelCompletionGoal goal in goals)
            {
                result = result && goal.IsGoalReached;
            }

            Debug.Log("Game is won: " + result);
            isGameWon = result;

            isGameOver = isGameOver || isGameWon;
            SetGamePaused(isGameOver);
        }

        public void TimeElapsed()
        {
            Debug.Log("Time elapsed");
            isGameOver = true;

            if (isGameWon)
            {
                Debug.Log("Game won");
                gameController.SetCurrentLevelWon();
            }
            else
            {
                Debug.Log("Game lost");
            }

            SetGamePaused(true);
        }

        public void SetGamePaused(bool isPaused)
        {
            pauseController.SetGamePaused(isPaused);

            if (isGameOver)
            {
                if (isGameWon)
                {
                    WinMenu.SetActive(isPaused);
                }
                else
                {
                    LoseMenu.SetActive(isPaused);
                }
            }
            else
            {
                PauseMenu.SetActive(isPaused);
            }
        }
    }
}