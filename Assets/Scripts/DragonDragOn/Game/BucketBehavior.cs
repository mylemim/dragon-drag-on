﻿using UnityEngine;
using System.Collections;
using DragonDragOn.Game;

namespace DragonDragOn.Game
{
    public class BucketBehavior : MonoBehaviour
    {
        private SpriteRenderer spriteRenderer;

        public Color Color
        {
            get
            {
                return spriteRenderer.color;
            }
        }

        public DragonType SupportedDragonType;

        void Awake()
        {
            spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        }
    }
}

