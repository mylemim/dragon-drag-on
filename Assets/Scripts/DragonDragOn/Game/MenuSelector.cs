﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace DragonDragOn.Game
{
    public class MenuSelector : MonoBehaviour
    {
        private GameController gameController;

        private void Start()
        {
            gameController = FindObjectOfType<GameController>();
        }

        public void NewGame()
        {
            SceneManager.LoadScene(gameController.CurrentLevelMetadata.SceneName);
        }

        public void RestartCurrentLevel()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        public void LoadMainMenu()
        {
            SceneManager.LoadScene("MainMenu");
        }

        public void LoadNextLevel()
        {
            if (gameController.HasNextLevel)
            {
                gameController.SetCurrentLevel(gameController.CurrentLevel + 1);
            }

            RestartCurrentLevel();
        }

        public void ExitGame()
        {
            Application.Quit();
        }
    }
}
