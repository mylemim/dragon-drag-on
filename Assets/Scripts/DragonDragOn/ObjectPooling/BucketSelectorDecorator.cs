﻿using UnityEngine;
using Common.DragAndDrop;
using Common.ObjectPooling;
using DragonDragOn.DragAndDrop;
using DragonDragOn.Game;

namespace DragonDragOn.ObjectPooling
{
    /// <summary>
    /// Decorates spawned dragons in order to aim them at a specific bucket. Each dragon will be assigned one bucket to be placed in.
    /// </summary>
    public class BucketSelectorDecorator : SpawnDecorator
    {
        private BucketBehavior[] bucketBehaviors;

        private void Awake()
        {
            bucketBehaviors = GameObject.FindObjectsOfType<BucketBehavior>();
        }

        public override void DecorateSpawnedObject(GameObject spawnedObject)
        {
            BucketBehavior selectedBehavior = bucketBehaviors[UnityEngine.Random.Range(0, bucketBehaviors.Length)];
            GameObject bucket = selectedBehavior.gameObject;

            // Set the selected bucket as the target
            DragAndDropControl control = spawnedObject.GetComponent<DragAndDropControl>();
            control.TargetGameObject = bucket;

            // Set the color of the bucket so we can match the dragon and the bucket visually
            SpriteRenderer spawnedObjectSpriteRenderer = spawnedObject.GetComponent<SpriteRenderer>();
            spawnedObjectSpriteRenderer.color = selectedBehavior.Color;
        }
    }
}
