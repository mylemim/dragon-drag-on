﻿using UnityEngine;
using Common.DragAndDrop;
using DragonDragOn.Game;
using Common.ObjectPooling;
using Common.Game.Score;
using UnityEditor.Events;
using UnityEngine.Events;

namespace DragonDragOn.DragAndDrop
{
    public class BucketDropBehavior : DragAndDropDecorator
    {
        private AudioSource soundSource;

        public CurrentScoreTracker ScoreManager;

        void Awake()
        {
            soundSource = gameObject.GetComponent<AudioSource>();
        }

        public override void DecorateGameObject(GameObject decoratedGameObject)
        {
            // Play the death/poof sound before destroying the object
            soundSource.Play();

            ScoreManager.AddScore(decoratedGameObject);
            DestroyObject(decoratedGameObject);
        }

        private static void DestroyObject(GameObject decoratedGameObject)
        {
            DeathController deathController = decoratedGameObject.GetComponent<DeathController>();
            if (deathController != null)
            {
                deathController.Die();
            }
            else
            {
                Destroy(decoratedGameObject);
            }
        }
    }
}
