﻿using Common.ObjectPooling;
using UnityEditor;


[CustomEditor(typeof(SpawnManager))]
public class SpawnManagerEditor : Editor
{

    public override void OnInspectorGUI()
    {
        SpawnManager myTarget = (SpawnManager)target;

        //myTarget.experience = EditorGUILayout.IntField("Experience", myTarget.experience);
        //EditorGUILayout.LabelField("Level", myTarget.Level.ToString());

        DrawDefaultInspector();

        EditorGUILayout.Space();

        myTarget.WaitingTimeBetweenSpawns = EditorGUILayout.FloatField("Waiting Time Between Spawns", myTarget.WaitingTimeBetweenSpawns);
        myTarget.SpawnFrequency = EditorGUILayout.FloatField("Spawn Frequency", myTarget.SpawnFrequency);
    }
}
